<?php

return array(
	'token'		=> env( 'LV_TOKEN', false ),
	'enable'	=> env( 'LV_ENABLE', false ),
	'url'		=> env( 'LV_URL', false )
);