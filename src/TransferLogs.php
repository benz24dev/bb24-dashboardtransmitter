<?php

namespace Bb24\Dashboardtransmitter;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TransferLogs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $arrErrors	= array();



    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $arrErrors )
    {
        $this->arrErrors	= $arrErrors;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		$ch 		= curl_init();
		$sUrl		= config( 'lv.url' ) . '/bulk';

		$arrPostParams	= array();
		foreach( $this->arrErrors as $arrError ) {
			$arrPostParams[]	= json_encode( $arrError );
		}

		\Log::info( $arrPostParams );

		curl_setopt($ch, CURLOPT_URL, $sUrl );
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

		curl_setopt($ch, CURLOPT_TIMEOUT_MS, 5000);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTREDIR, 3);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $arrPostParams );
//		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0 );
		$result = curl_exec($ch);
		curl_close($ch);


		\Log::info( 'Fehler an LV übermittelt Status: ' . json_encode( $result, true ) );
	}
}
