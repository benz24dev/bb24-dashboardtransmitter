<?php

namespace Bb24\Dashboardtransmitter;

use Bb24\Dashboardtransmitter\DashboardtransmitterExceptionHandler;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Support\ServiceProvider;

class DashboardtransmitterServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
		$this->app->bind( ExceptionHandler::class, DashboardtransmitterExceptionHandler::class );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
		$this->mergeConfigFrom(__DIR__.'/lv.php', 'lv' );
    }
}
