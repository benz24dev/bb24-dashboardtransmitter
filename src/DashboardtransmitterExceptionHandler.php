<?php

namespace Bb24\Dashboardtransmitter;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Container\Container;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class DashboardtransmitterExceptionHandler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthenticationException::class
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];


    protected $arrAllowedServerKeys	= array(
		'UNIQUE_ID',
		'HTTP_ACCEPT',
		'HTTP_ACCEPT_LANGUAGE',
		'HTTP_ACCEPT_ENCODING',
		'HTTP_USER_AGENT',
		'HTTP_HOST',
		'HTTP_CONNECTION',
		'SystemRoot',
		'COMSPEC',
		'PATHEXT',
		'WINDIR',
		'SERVER_SIGNATURE',
		'SERVER_SOFTWARE',
		'SERVER_NAME',
		'SERVER_ADDR',
		'SERVER_PORT',
		'REMOTE_ADDR',
		'DOCUMENT_ROOT',
		'SERVER_ADMIN',
		'SCRIPT_FILENAME',
		'REMOTE_PORT',
		'GATEWAY_INTERFACE',
		'SERVER_PROTOCOL',
		'REQUEST_METHOD',
		'QUERY_STRING',
		'REQUEST_URI',
		'SCRIPT_NAME',
		'PHP_SELF',
		'REQUEST_TIME',
	);


	/**
	 * @var array $arrErrors
	 */
    protected $arrErrors	= array();



    public function __construct( Container $container ) {
		parent::__construct( $container );

		$this->arrAllowedServerKeys	= array_combine( $this->arrAllowedServerKeys, $this->arrAllowedServerKeys );
	}



    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Throwable $exception
     *
     * @return void
     * @throws Throwable
     */
    public function report(Throwable $exception)
    {
		parent::report($exception);

		$this->recognize($exception);
    }



    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param Throwable                $exception
     *
     * @return \Illuminate\Http\Response
     * @throws Throwable
     */
    public function render($request, Throwable $exception)
    {
        return parent::render($request, $exception);
    }



    /**
     *
     * @author     l.brinkmann
     * @since      1.0
     *
     * @param Throwable $exception
     *
     * @version    1.0
     */
	public function recognize( Throwable $exception ) {
		if( $this->loggingEnabled() == true ) {
			$arrRequest		= array_diff_key( request()->except($this->dontFlash) ?? [], $_COOKIE ?? [] );

			$arrPostData	= array(
				'LV_TOKEN'				=> config( 'lv.token' ),
				'LV_TYPE'				=> 'php_error',
				'LV_TIMESTAMP'			=> time(),
				'LV_PAYLOAD'			=> array(
					'request'				=> $this->extractArray( $arrRequest, 2 ),
					'server'				=> $this->getServerVars(),
					'errstr'				=> $exception->getMessage(),
					'errfile'				=> $exception->getFile(),
					'errno'					=> $exception->getCode(),
					'errline'				=> $exception->getLine(),
				)
			);

			$this->arrErrors[]	= $arrPostData;
			if( count( $this->arrErrors ) >= 50 ) {
				$this->prepareLogsForTransfer();
				$this->arrErrors	= array();
			}
		}
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version master
	 *
	 * @param     $arrData
	 * @param int $maxDepth
	 * @param int $curDepth
	 *
	 * @return  string
	 */
	protected function extractArray( $arrData, $maxDepth=2, $curDepth=0 ) {
		if( $curDepth > $maxDepth ) return 'Array..';
		foreach( $arrData as $key => $mixedValue ) {
			if( is_array( $mixedValue ) == true ) {
				$arrData[ $key ]	= self::extractArray( $mixedValue, $maxDepth, $curDepth + 1 );
			}
		}

		return $arrData;
	}



	public function __destruct() {
		$this->prepareLogsForTransfer();
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 */
	protected function prepareLogsForTransfer() {
		if( empty( $this->arrErrors ) == false ) {
			\Log::info( 'Anzahl Fehler: ' . count( $this->arrErrors ) );
			if( $this->loggingEnabled() == true ) {
				$this->sendToDashboard();
			}
		}
	}




	/**
	 * Legt den Job in die queue
	 *
	 * @author  l.brinkmann
	 * @since	1.0
	 * @version	1.0
	 *
	 */
	protected function sendToDashboard() {
		TransferLogs::dispatch( $this->arrErrors );
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	1.0
	 * @version	1.0
	 *
	 * @return  bool
	 */
	protected function loggingEnabled() {
		$blnEnabled	= false;
		if( config( 'lv.token' ) == true && config( 'lv.enable' ) == true ) {
			$blnEnabled	= true;
		}

		return $blnEnabled;
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	1.0
	 * @version	1.0
	 *
	 * @return  array
	 */
	protected function getServerVars() {
		$arrServer	= array();
		foreach( \Request::server() as $key => $value ) {
			if( isset( $this->arrAllowedServerKeys[ $key ] ) == true ) {
				$arrServer[ $key ]	= $value;
			}
		}

		return $arrServer;
	}

}
